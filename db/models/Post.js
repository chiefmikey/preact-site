import mongoose from 'mongoose';

const modelPost = new mongoose.Schema({
  emotion: String,
  normalEmotion: String,
  emoEmotion: String,
  animal: String,
  petName: String,
  birthday: String,
  fruit: String,
  sugar: String,
  mainResult: String,
  emoResult: String,
});

export default modelPost;
