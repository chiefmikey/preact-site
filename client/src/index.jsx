import { h, render } from 'preact';
import { createBrowserHistory } from 'history';
import App from './components/App.jsx';
import './style/index.css';

const history = createBrowserHistory();

render(<App history={history} />, document.body);
