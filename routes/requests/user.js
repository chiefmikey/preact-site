/* eslint-disable no-param-reassign */
import Router from '@koa/router';
import { saveUser } from '../../db/queries/save.js';
import { findUser } from '../../db/queries/find.js';

const router = new Router({ prefix: '/user' });

router.get('/', async (ctx) => {
  try {
    const results = await findUser(
      ctx.request.query.something1,
      ctx.request.query.something2,
      ctx.request.query.something3,
    );
    ctx.response.status = 200;
    ctx.response.body = results;
  } catch (error) {
    console.error('error with get', error);
    ctx.response.status = 200;
  }
});

router.post('/', async (ctx) => {
  try {
    const results = await saveUser(ctx.request.query.something);
    ctx.response.status = 200;
    ctx.response.body = results;
  } catch (error) {
    console.error('error with post', error);
    ctx.response.status = 200;
  }
});

export default router;
