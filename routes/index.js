import Router from '@koa/router';
import user from './requests/user.js';
import process from './requests/process.js';

const router = new Router({ prefix: '/' });

router.use(
  user.routes(),
  user.allowedMethods(),
  process.routes(),
  process.allowedMethods(),
);

export default router;
