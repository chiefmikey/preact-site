/* eslint-disable no-param-reassign */
import Router from '@koa/router';
import console from '../helpers/console.js';

const router = new Router({ prefix: '/process' });

router.get('/', async (ctx) => {
  try {
    const results = await console(ctx.request.query.something);
    ctx.response.status = 200;
    ctx.response.body = results;
  } catch (error) {
    console.error('error with get', error);
    ctx.response.status = 200;
  }
});

export default router;
