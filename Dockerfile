FROM alpine:latest
ENV PORT=8080
EXPOSE 8080
WORKDIR /preact-site
COPY . .
COPY init.sh /bin
RUN rm /preact-site/init.sh
RUN chmod +x /bin/init.sh
ENTRYPOINT "init.sh"
