import mongoose from 'mongoose';
import modelUser from './models/User.js';
import modelPost from './models/Post.js';

const mongoURI = 'mongodb://name-generator_db:27017/db';

export const db = mongoose.connect(mongoURI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  // useFindAndModify: false,
  // useCreateIndex: true,
});

db.then(() => console.log(`Connected to: ${mongoURI}`)).catch((error) => {
  console.error(
    `There was a problem connecting to mongo at: ${mongoURI}`,
    error,
  );
});

export const User = mongoose.model('User', modelUser);
export const Post = mongoose.model('Post', modelPost);
